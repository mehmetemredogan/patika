package com.mehmetemredogan;

import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String dogumGunu;

        Scanner input = new Scanner(System.in);

        System.out.print("Doğum gününüzü girin (Örnek: 27 Haziran): ");
        dogumGunu = input.nextLine();

        String[] dogumGunuB = dogumGunu.split(" ");

        if (Objects.equals(dogumGunuB[1], "Aralık")) {
            if (Integer.parseInt(dogumGunuB[0]) >= 1 && Integer.parseInt(dogumGunuB[0]) <= 20) {
                System.out.print("Burcunuz: Yay");
            } else if (Integer.parseInt(dogumGunuB[0]) <= 31) {
                System.out.print("Burcunuz: Oğlak");
            } else {
                System.out.print("Aralık ayı 31 gündür. Gün bilgisini kontrol edin.");
            }
        }

        if (Objects.equals(dogumGunuB[1], "Ocak")) {
            if (Integer.parseInt(dogumGunuB[0]) >= 1 && Integer.parseInt(dogumGunuB[0]) <= 21) {
                System.out.print("Burcunuz: Oğlak");
            } else if (Integer.parseInt(dogumGunuB[0]) <= 30) {
                System.out.print("Burcunuz: Kova");
            } else {
                System.out.print("Ocak ayı 30 gündür. Gün bilgisini kontrol edin.");
            }
        }

        if (Objects.equals(dogumGunuB[1], "Şubat")) {
            if (Integer.parseInt(dogumGunuB[0]) >= 1 && Integer.parseInt(dogumGunuB[0]) <= 19) {
                System.out.print("Burcunuz: Kova");
            } else if (Integer.parseInt(dogumGunuB[0]) <= 29) {
                System.out.print("Burcunuz: Balık");
            } else {
                System.out.print("Şubat ayı en fazla 29 gündür. Gün bilgisini kontrol edin.");
            }
        }

        if (Objects.equals(dogumGunuB[1], "Mart")) {
            if (Integer.parseInt(dogumGunuB[0]) >= 1 && Integer.parseInt(dogumGunuB[0]) <= 20) {
                System.out.print("Burcunuz: Balık");
            } else if (Integer.parseInt(dogumGunuB[0]) <= 31) {
                System.out.print("Burcunuz: Koç");
            } else {
                System.out.print("Mart ayı 31 gündür. Gün bilgisini kontrol edin.");
            }
        }

        if (Objects.equals(dogumGunuB[1], "Nisan")) {
            if (Integer.parseInt(dogumGunuB[0]) >= 1 && Integer.parseInt(dogumGunuB[0]) <= 20) {
                System.out.print("Burcunuz: Koç");
            } else if (Integer.parseInt(dogumGunuB[0]) <= 30) {
                System.out.print("Burcunuz: Boğa");
            } else {
                System.out.print("Nisan ayı 30 gündür. Gün bilgisini kontrol edin.");
            }
        }

        if (Objects.equals(dogumGunuB[1], "Mayıs")) {
            if (Integer.parseInt(dogumGunuB[0]) >= 1 && Integer.parseInt(dogumGunuB[0]) <= 21) {
                System.out.print("Burcunuz: Boğa");
            } else if (Integer.parseInt(dogumGunuB[0]) <= 31) {
                System.out.print("Burcunuz: İkizler");
            } else {
                System.out.print("Mayıs ayı 31 gündür. Gün bilgisini kontrol edin.");
            }
        }

        if (Objects.equals(dogumGunuB[1], "Haziran")) {
            if (Integer.parseInt(dogumGunuB[0]) >= 1 && Integer.parseInt(dogumGunuB[0]) <= 22) {
                System.out.print("Burcunuz: İkizler");
            } else if (Integer.parseInt(dogumGunuB[0]) <= 30) {
                System.out.print("Burcunuz: Yengeç");
            } else {
                System.out.print("Haziran ayı 30 gündür. Gün bilgisini kontrol edin.");
            }
        }

        if (Objects.equals(dogumGunuB[1], "Temmuz")) {
            if (Integer.parseInt(dogumGunuB[0]) >= 1 && Integer.parseInt(dogumGunuB[0]) <= 22) {
                System.out.print("Burcunuz: Yengeç");
            } else if (Integer.parseInt(dogumGunuB[0]) <= 31) {
                System.out.print("Burcunuz: Aslan");
            } else {
                System.out.print("Temmuz ayı 31 gündür. Gün bilgisini kontrol edin.");
            }
        }

        if (Objects.equals(dogumGunuB[1], "Ağustos")) {
            if (Integer.parseInt(dogumGunuB[0]) >= 1 || Integer.parseInt(dogumGunuB[0]) <= 22) {
                System.out.print("Burcunuz: Aslan");
            } else if (Integer.parseInt(dogumGunuB[0]) <= 31) {
                System.out.print("Burcunuz: Başak");
            } else {
                System.out.print("Ağustos ayı 31 gündür. Gün bilgisini kontrol edin.");
            }
        }

        if (Objects.equals(dogumGunuB[1], "Eylül")) {
            if (Integer.parseInt(dogumGunuB[0]) >= 1 || Integer.parseInt(dogumGunuB[0]) <= 22) {
                System.out.print("Burcunuz: Başak");
            } else if (Integer.parseInt(dogumGunuB[0]) <= 30) {
                System.out.print("Burcunuz: Terazi");
            } else {
                System.out.print("Eylül ayı 30 gündür. Gün bilgisini kontrol edin.");
            }
        }

        if (Objects.equals(dogumGunuB[1], "Ekim")) {
            if (Integer.parseInt(dogumGunuB[0]) >= 1 || Integer.parseInt(dogumGunuB[0]) <= 22) {
                System.out.print("Burcunuz: Terazi");
            } else if (Integer.parseInt(dogumGunuB[0]) <= 31) {
                System.out.print("Burcunuz: Akrep");
            } else {
                System.out.print("Ekim ayı 31 gündür. Gün bilgisini kontrol edin.");
            }
        }

        if (Objects.equals(dogumGunuB[1], "Kasım")) {
            if (Integer.parseInt(dogumGunuB[0]) >= 1 || Integer.parseInt(dogumGunuB[0]) <= 21) {
                System.out.print("Burcunuz: Akrep");
            } else if (Integer.parseInt(dogumGunuB[0]) <= 30) {
                System.out.print("Burcunuz: Yay");
            } else {
                System.out.print("Kasım ayı 30 gündür. Gün bilgisini kontrol edin.");
            }
        }


    }
}
