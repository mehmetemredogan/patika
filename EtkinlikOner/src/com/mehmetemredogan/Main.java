package com.mehmetemredogan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int hava;

        Scanner input = new Scanner(System.in);
        System.out.print("Hava sıcaklığını girin: ");
        hava = input.nextInt();

        if (hava < -25 || hava > 45) {
            System.out.print(":)");
            System.exit(1);
        }

        if (hava < 5) {
            System.out.print("Kayak Yap");
            System.exit(1);
        }

        if (hava < 15) {
            System.out.print("Sinemaya git");
            System.exit(1);
        }

        if (hava < 25) {
            System.out.print("Pikniğe git");
            System.exit(1);
        }

        if (hava > 25) {
            System.out.print("Yüzmeye git");
            System.exit(1);
        }
    }
}
