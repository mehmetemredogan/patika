package mehmetemredogan;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String username, password;
        int right = 3, bakiye = 1500;
        Scanner input = new Scanner(System.in);

        System.out.println("Hoşgeldiniz!");

        while (right > 0) {
            System.out.print("Kullanıcı Adınız: ");
            username = input.nextLine();

            System.out.print("Parolanız: ");
            password = input.nextLine();

            if (!username.equals("mehmetemredogan") && !password.equals("123")) {
                System.out.println("Kullanıcı Adı veya Parola Hatalı!");
                right--;
                System.out.printf("Kalan Deneme: %d", right);
                return;
            } else {
                break;
            }
        }

        int durdur = 0;
        while (durdur == 0) {
            System.out.print(
                """
                İşlem Seçin:
                1- Para Yatırma
                2- Para Çekme
                3- Bakiye Sorgulama
                4- Çıkış
                """
            );
            int islem = input.nextInt();
    
            switch (islem) {
                case 1:
                    // Para Yatır
                    System.out.print("Yatırılan Tutarı Girin: ");
                    int yatirilan = input.nextInt();
                    bakiye += yatirilan;
                    System.out.printf("Yatırılan Tutar: %d\n", yatirilan);
                    System.out.printf("Güncel Bakiyeniz: %d\n", bakiye);
                    break;
                case 2:
                    // Para Çek
                    System.out.print("Çekilen Tutarı Girin: ");
                    int cekilen = input.nextInt();
                    if (cekilen > bakiye) {
                        System.out.println("Yetersiz bakiye!");
                        break;
                    }
                    bakiye -= cekilen;
                    System.out.printf("Çekilen Tutar: %d\n", cekilen);
                    System.out.printf("Güncel Bakiyeniz: %d\n", bakiye);
                    break;
                case 3:
                    System.out.printf("Güncel Bakiyeniz: %d\n", bakiye);
                    break;
                case 4:
                    durdur++;
                    break;
                default:
                    System.out.println("Hatalı kod girdiniz");
                    break;
            }
        }
    }
}
