package com.mehmetemredogan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int yil;

        Scanner input = new Scanner(System.in);

        System.out.print("Yıl girin: ");
        yil = input.nextInt();

        if ((yil % 4) == 0 || (yil % 400) == 0) {
            System.out.printf("%d artık yıldır", yil);
        } else {
            System.out.printf("%d artık yıl değildir", yil);
        }
    }
}
