package com.mehmetemredogan;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int sayi;
        Scanner input = new Scanner(System.in);

        System.out.print("Sayı: ");
        sayi = input.nextInt();

        asal(sayi, 2);
    }

    static void asal(int sayi, int i) {
        if (i >= sayi) {
            System.out.printf("%d sayısı asaldır.", sayi);
            return;
        }

        if (sayi % i == 0) {
            System.out.printf("%d sayısı asal değildir.", sayi);
        } else {
            asal(sayi, i + 1);
        }
    }
}
