package org.mehmetemredogan;

public class Main {
    public static void main(String[] args) {
        String[][] letter = new String[7][12];

        for (int i = 0; i < letter.length; i++) {
            for (int j = 0; j < letter[i].length; j++) {
                if (i == 0) {
                    if (j == 11) {
                        letter[i][j] = " ";
                    } else {
                        letter[i][j] = "*";
                    }
                } else if (i == 1 || i == 2) {
                    if (j == 0 || j == 11) {
                        letter[i][j] = "*";
                    } else {
                        letter[i][j] = " ";
                    }
                } else if (i == 3) {
                    if (j <= 10) {
                        letter[i][j] = "*";
                    } else {
                        letter[i][j] = " ";
                    }
                } else if (i == 4 || i == 5) {
                    if (j == 0 || j == 11) {
                        letter[i][j] = "*";
                    } else {
                        letter[i][j] = " ";
                    }
                } else {
                    if (j == 11) {
                        letter[i][j] = " ";
                    } else {
                        letter[i][j] = "*";
                    }
                }
            }
        }

        for (String[] row : letter){
            for (String col : row){
                System.out.print(col);
            }
            System.out.println();
        }
    }
}