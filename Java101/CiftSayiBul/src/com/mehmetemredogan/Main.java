package com.mehmetemredogan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int sayi;

        Scanner input = new Scanner(System.in);

        System.out.print("Bir sayı girin: ");
        sayi = input.nextInt();

        System.out.print("Çift Sayılar: ");
        for (int i  = 0; i <= sayi; i++) {
            if (i % 2 == 0) {
                System.out.print(i + ",");
            }
        }
    }
}
