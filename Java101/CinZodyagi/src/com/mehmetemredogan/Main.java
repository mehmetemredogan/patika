package com.mehmetemredogan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int yil;

        System.out.print("Doğum yılınızı girin: ");

        Scanner input = new Scanner(System.in);
        yil = input.nextInt();

        if (yil % 12 == 0) {
            System.out.print("Maymun");
        } else if (yil % 12 == 1) {
            System.out.print("Horoz");
        } else if (yil % 12 == 2) {
            System.out.print("Köpek");
        } else if (yil % 12 == 3) {
            System.out.print("Domuz");
        } else if (yil % 12 == 4) {
            System.out.print("Fare");
        } else if (yil % 12 == 5) {
            System.out.print("Öküz");
        } else if (yil % 12 == 6) {
            System.out.print("Kaplan");
        } else if (yil % 12 == 7) {
            System.out.print("Tavşan");
        } else if (yil % 12 == 8) {
            System.out.print("Ejderha");
        } else if (yil % 12 == 9) {
            System.out.print("Yılan");
        } else if (yil % 12 == 10) {
            System.out.print("At");
        } else if (yil % 12 == 11) {
            System.out.print("Koyun");
        } else {
            System.out.print("Bir hata var!");
        }
    }
}
