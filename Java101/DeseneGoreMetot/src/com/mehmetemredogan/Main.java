package com.mehmetemredogan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int sayi;
        Scanner input = new Scanner(System.in);

        System.out.print("Sayı: ");
        sayi = input.nextInt();

        desen(sayi, sayi, false);
    }

    static void desen(int sayi, int temp, boolean donus) {
        System.out.printf("%d ", temp);

        if (temp <= 0) {
            desen(sayi,temp + 5, true);
        }

        if (donus) {
            if (temp >= sayi) {
                System.exit(1);
            } else {
                desen(sayi, temp + 5, true);
            }
        }

        if (!donus) {
            desen(sayi, temp - 5, false);
        }
    }
}
