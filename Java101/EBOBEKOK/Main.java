import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int n1, n2, ebob = 0;
        double ekok;

        Scanner input = new Scanner(System.in);
        
        System.out.print("n1 değerini girin :");
        n1 = input.nextInt();

        System.out.print("n2 değerini girin :");
        n2 = input.nextInt();

        // EBOB
        int i = 1;
        while (i <= n1) {
            if ((n1 % i == 0) && (n2 % i == 0)) {
                ebob = i;
            }
            i++;
        }

        // EKOK
        ekok = (n1 * n2) / ebob;

        System.out.printf("EBOB: %d\n", ebob);
        System.out.printf("EKOK: %f", ekok);
    }
}
