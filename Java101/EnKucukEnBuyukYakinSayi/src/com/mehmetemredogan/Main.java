package com.mehmetemredogan;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int[] list = {15, 12, 788, 1, -1, -778, 2, 0};
        int temp;

        // Diziyi küçükten büyüğe sırala
        for (int i = 0; i < list.length; i++) {
            for (int j = i + 1; j < list.length; j++) {
                if (list[i] > list[j]) {
                    temp = list[i];
                    list[i] = list[j];
                    list[j] = temp;
                }
            }
        }

        // Sayıyı al
        Scanner input = new Scanner(System.in);
        System.out.print("Bir sayi girin: ");
        int number = input.nextInt();

        // Sayı istenen aralıkta mı?
        if ((number < list[0]) || number > (list[(list.length) - 1])) {
            System.out.println("Girdiginiz sayi dizi araliginin disinda");
            System.exit(-1);
        }

        int minClosest = list[0], maxClosest = list[list.length - 1];

        for (int i: list) {
            if (i < number) {
                minClosest = i;
            }
        }

        for (int i = list.length - 1; i >= 0; i--) {
            if (list[i] > number) {
                maxClosest = list[i];
            }
        }

        System.out.println("Dizi: " + Arrays.toString(list));
        System.out.println("Girilen Deger: " + number);
        System.out.println("Girilen sayidan kucuk en yakin sayi: " + minClosest);
        System.out.println("Girilen sayidan buyuk en yakin sayi " + maxClosest);
    }
}
