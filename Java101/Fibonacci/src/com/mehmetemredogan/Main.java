package com.mehmetemredogan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int sayi;
        Scanner input = new Scanner(System.in);

        System.out.print("Eleman sayısını girin: ");
        sayi = input.nextInt();

        int sayiA = 0;
        int sayiB = 1;

        int beforeNumber = sayiB, toplam = sayiA + sayiB;
        System.out.printf("%d %d %d ", sayiA, sayiB, toplam);

        for (int i = 3; i < sayi; i++) {
            int oncekiToplam = toplam;
            toplam = beforeNumber + toplam;
            beforeNumber = oncekiToplam;
            System.out.printf("%d ", toplam);
        }
    }
}
