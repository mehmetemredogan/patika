package com.mehmetemredogan;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        List<Double> numbers = new ArrayList<>();
        double number, sum = 0;
        Scanner input = new Scanner(System.in);

        while (true) {
            System.out.print("Bir sayı girin: ");
            number = input.nextDouble();
            if (number == 0) {
                break;
            } else {
                numbers.add(number);
            }
        }

        for (Double num : numbers) {
            sum += 1 / num;
        }

        System.out.printf("%s sayılarının harmonik ortalaması: %f", numbers, (numbers.size() / sum));
    }
}
