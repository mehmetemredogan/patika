package com.mehmetemredogan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int sayi;
        double sonuc = 1;

        Scanner input = new Scanner(System.in);

        System.out.print("Bir sayı girin: ");
        sayi = input.nextInt();

        for (double i = 2; i <= sayi; i++) {
            sonuc += (1/i);
        }

        System.out.print(sonuc);
    }
}
