package com.mehmetemredogan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int sayi, lock = 0;
        StringBuilder dordunkuvvetleri = new StringBuilder();
        StringBuilder besinkuvvetleri = new StringBuilder();

        Scanner input = new Scanner(System.in);
        System.out.print("Bir sayı girin: ");
        sayi = input.nextInt();

        for (int i = 0; i < sayi; i++) {
            if (Math.pow(4,i) >= sayi) {
                lock++;
            } else {
                dordunkuvvetleri.append((int) Math.pow(4, i)).append(" ");
            }

            if (Math.pow(5,i) >= sayi) {
                lock++;
            } else {
                besinkuvvetleri.append((int) Math.pow(5, i)).append(" ");
            }

            if (lock == 2) {
                break;
            }
        }

        System.out.println("Dördün Kuvvetleri: " + dordunkuvvetleri);
        System.out.println("Beşin Kuvvetleri: " + besinkuvvetleri);
    }
}
