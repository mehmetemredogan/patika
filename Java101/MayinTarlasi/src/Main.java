import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Hello! Please specify the playing field in rows and columns.");

        System.out.print("Row: ");
        int row = input.nextInt();

        System.out.print("Column: ");
        int column = input.nextInt();

        if (row < 4 || column < 4) {
            System.out.println("Chush! You must create an area of at least 4x4!");
            System.exit(-1);
        }

        MineSweeper mineSweeper = new MineSweeper(row, column);
        mineSweeper.run();
    }
}