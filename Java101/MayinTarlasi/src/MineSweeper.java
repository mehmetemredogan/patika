import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class MineSweeper {
    int row;
    int column;

    public MineSweeper(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public void run() {
        hr();

        int minMine = minimumMineCalculator();
        int[][] area = new int[this.row][this.column];
        String[][] visualArea = new String[this.row][this.column];
        Random random = new Random();

        for (int i = 0; i < this.row; i++) {
            for (int j = 0; j < this.column; j++) {
                int r = random.nextInt(500);
                if ((minMine > 0) && (r % 2 == 0)) {
                    area[i][j] = 1;
                    visualArea[i][j] = " * ";
                    minMine--;
                } else {
                    area[i][j] = 0;
                    visualArea[i][j] = " - ";
                }
            }
        }

        Scanner input = new Scanner(System.in);

        for (int i = 0; i < (this.row * this.column); i++) {
            System.out.print("Row: ");
            int r = input.nextInt();

            System.out.print("Column: ");
            int c = input.nextInt();

            if (r >= this.row || c >= this.column) {
                System.out.println("You cannot leave the area!");
                continue;
            }

            if (area[r][c] == 1) {
                System.out.println("Game Over!");
                printArea(visualArea);
                break;
            } else {
                System.out.println("Continue...");
            }
        }

        hr();
    }

    private int minimumMineCalculator() {
        int area = (this.row * this.column) / 4;
        return Math.round(area);
    }

    private void printArea(String[][] visualArea) {
        for (int i = 0; i < this.row; i++) {
            for (int j = 0; j < this.column; j++) {
                System.out.print(visualArea[i][j]);
            }
            System.out.print('\n');
        }
    }

    private void hr() {
        for (int i = 0; i < (this.row * 3); i++) {
            System.out.print("-");
        }
        System.out.print("\n");
    }
}
