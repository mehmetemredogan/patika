package com.mehmetemredogan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int adet;
        Scanner input = new Scanner(System.in);

        System.out.print("Kaç adet sayı gireceksiniz? ");
        adet = input.nextInt();

        int[] sayilar = new int[adet];

        for (int i = 1; i <= adet; i++) {
            System.out.printf("%d. sayıyı girin: ", i);
            sayilar[i - 1] = input.nextInt();
        }

        int enkucuk = sayilar[0];
        int enbuyuk = sayilar[0];
        for (int i = 0; i < adet; i++) {
            if (sayilar[i] < enkucuk) {
                enkucuk = sayilar[i];
            }

            if (sayilar[i] > enbuyuk) {
                enbuyuk = sayilar[i];
            }
        }

        System.out.println("En küçük sayı: " + enkucuk);
        System.out.println("En büyük sayı: " + enbuyuk);
    }
}
