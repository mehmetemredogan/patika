package com.mehmetemredogan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int sayi, toplam = 0;
        Scanner input = new Scanner(System.in);

        System.out.print("Bir sayı girin: ");
        sayi = input.nextInt();

        for (int i = 1; i < sayi; i++) {
            if (sayi % i == 0) {
                toplam += i;
            }
        }

        if (sayi == toplam) {
            System.out.printf("%d sayısı mükemmel sayıdır.", sayi);
        } else {
            System.out.printf("%d sayısı mükemmel sayı değildir.", sayi);
        }
    }
}
