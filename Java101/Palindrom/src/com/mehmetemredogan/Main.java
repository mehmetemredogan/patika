package com.mehmetemredogan;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int number, tempNumber, numberCopy, reverseNumber;
        StringBuilder ters = new StringBuilder();

        Scanner input = new Scanner(System.in);

        System.out.print("Bir sayı girin: ");
        number = tempNumber = numberCopy = input.nextInt();

        while (tempNumber != 0) {
            ters.append(number % 10);
            tempNumber /= 10;
            number = number / 10;
        }

        reverseNumber = Integer.parseInt(String.valueOf(ters));

        if (numberCopy == reverseNumber) {
            System.out.printf("%d sayısı palindromdur.", reverseNumber);
        } else {
            System.out.printf("%d sayısı palindrom değildir.", reverseNumber);
        }
    }
}
