import java.util.Scanner;

public class Main {
    static boolean isPalindrome(String str) {
        StringBuilder reverse = new StringBuilder();
        for (int i = str.length() - 1; i >= 0; i--) {
            reverse.append(str.charAt(i));
        }

        return str.equals(reverse.toString());
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Bir kelime giriniz");
        String data = input.nextLine();

        System.out.println(isPalindrome(data));
    }
}