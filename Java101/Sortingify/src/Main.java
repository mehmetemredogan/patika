import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int loop;
        Scanner input = new Scanner(System.in);

        System.out.println("Bu dizi kaç elemanlı?");
        loop = input.nextInt();
        int[] list = new int[loop];

        for (int i = 0; i < loop; i++) {
            System.out.printf("%d. elemanı girin: \n", i);
            list[i] = input.nextInt();
        }

        for (int i = 0; i < list.length; i++) {
            System.out.printf("%d. eleman: %d\n", i, list[i]);
        }

        Arrays.sort(list);

        System.out.printf("Küçükten Büyüğe Sıralama : %s", Arrays.toString(list));
    }
}