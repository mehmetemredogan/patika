package com.mehmetemredogan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int sayi;
        System.out.print("Bir Sayı Giriniz :");

        Scanner input = new Scanner(System.in);
        sayi = input.nextInt();

        for (int i = (sayi - 1); 0 <= i; i--) {
            for (int j = i; j < sayi; j++) {
                System.out.print(" ");
            }
            for (int k = 1; k <= (2 * i + 1); k++) {
                System.out.print("*");
            }
            System.out.println(" ");
        }
    }
}
