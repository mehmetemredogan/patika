package com.mehmetemredogan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int mesafe, yas, tip;
        double mesafeBasiUcret = 0.1, toplam;

        Scanner input = new Scanner(System.in);

        System.out.print("Mesafeyi KM Türünden Giriniz: ");
        mesafe = input.nextInt();

        checkPositive(mesafe);

        toplam = (mesafe * mesafeBasiUcret);

        System.out.print("Yaşınız: ");
        yas = input.nextInt();

        checkPositive(yas);

        System.out.print(
                """
                1 - Tek Yön
                2 - Gidiş Dönüş
                Yolculuk Tipi: """
        );
        tip = input.nextInt();

        checkPositive(tip);

        if (tip != 1 && tip != 2) {
            System.out.print("Hatalı veri girdiniz!");
            System.exit(-1);
        }

        // 12 Yaş İndirimi
        if (yas <= 12) {
            toplam = toplam / 2;
        }

        // 12-24 Yaş İndirimi
        if (yas > 12 && yas <= 24) {
            toplam = toplam - (toplam * 0.1);
        }

        // 65 Yaş İndirimi
        if (yas > 65) {
            toplam = toplam - (toplam * 0.3);
        }

        // Gidiş Dönüş İndirimi (Toplam)
        if (tip == 2) {
            toplam = (toplam - (toplam * 0.2)) * 2;
        }

        System.out.printf("Toplam Tutar: %f TL", toplam);
    }

    public static void checkPositive(int number) {
        if (number < 0) {
            System.out.print("Değer pozitif tam sayı olmalıdır!");
            System.exit(-1);
        }
    }
}
