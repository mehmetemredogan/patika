package com.mehmetemredogan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int taban, us;
        Scanner input = new Scanner(System.in);

        System.out.print("Taban: ");
        taban = input.nextInt();

        System.out.print("Üs: ");
        us = input.nextInt();

        System.out.printf("Sonuç: %d", hesapla(taban, us));
    }

    public static int hesapla(int taban, int us) {
        if (us == 0) {
            return 1;
        }

        return taban * hesapla(taban, us - 1);
    }
}
