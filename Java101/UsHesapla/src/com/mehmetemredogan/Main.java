package com.mehmetemredogan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int taban, us, sonuc = 1;

        Scanner input = new Scanner(System.in);

        System.out.print("Taban değerini girin: ");
        taban = input.nextInt();

        System.out.print("Üs değerini girin: ");
        us = input.nextInt();

        for (int i = 1; i <= us; i++) {
            sonuc = sonuc * taban;
        }

        System.out.printf("Sonuc: %d", sonuc);
    }
}
