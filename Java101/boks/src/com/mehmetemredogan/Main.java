package com.mehmetemredogan;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Random rand = new Random();

        int random = rand.nextInt(2);

        int damage = rand.nextInt(18);
        int damageB = rand.nextInt(18);
        int health = rand.nextInt(80,100);
        int healthB = rand.nextInt(80,100);
        int weight = rand.nextInt(90,100);
        int weightB = rand.nextInt(90,100);

        Fighter marc = new Fighter("Marc" , damage , health, weight, 0);
        Fighter alex = new Fighter("Alex" , damageB , healthB, weightB, 0);

        if (random == 1) {
            Ring r = new Ring(marc, alex, 90 , 100);
            r.run();
        } else {
            Ring r = new Ring(alex, marc, 90 , 100);
            r.run();
        }
    }
}
