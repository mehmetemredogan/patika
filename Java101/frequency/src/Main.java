import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        int[] list = {10, 20, 20, 10, 10, 20, 5, 20};

        Map<String, Integer> freq = new HashMap<>();

        for (int i : list) {
            String value = String.valueOf(i);
            freq.merge(value, 1, Integer::sum);
        }

        System.out.printf("Dizi: %s\n", Arrays.toString(list));
        System.out.println("Tekrar Sayıları");

        for (var data : freq.entrySet()) {
            System.out.printf("%s sayısı %d kere tekrar edildi.\n", data.getKey(), data.getValue());
        }
    }
}