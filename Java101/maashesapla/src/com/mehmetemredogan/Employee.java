package com.mehmetemredogan;

public class Employee {
    String name;
    double salary;
    int workHours;
    int hireYear;

    public Employee (String name, double salary, int workHours, int hireYear) {
        this.name = name;
        this.salary = salary;
        this.workHours = workHours;
        this.hireYear = hireYear;
        hesapla();
    }

    public void hesapla() {
        double bonus = bonus();
        double raiseSalary = raiseSalary();
        double tax = tax();

        double taxAndBonus = this.salary + (bonus - tax);

        System.out.printf("Vergi: %f\n", tax);
        System.out.printf("Bonus: %f\n", bonus);
        System.out.printf("Maaş Artışı: %f\n", raiseSalary);
        System.out.printf("Vergi ve Bonuslar ile birlikte maaş: %f\n", taxAndBonus);

        System.out.printf("Yeni Maaş: %f\n", taxAndBonus + raiseSalary);
    }

    public double tax() {
        double tax;
        if (this.salary <= 1000) {
            tax = 0;
        } else {
            tax = salary * 0.03;
        }

        return tax;
    }

    public double bonus() {
        double bonus = 0;
        if (this.workHours > 40) {
            bonus = (this.workHours - 40) * 30;
        }

        return bonus;
    }

    public double raiseSalary() {
        double raise;
        int wy = 2021 - this.hireYear;

        if (wy <= 9) {
            raise = salary * 0.05;
        } else if (19 < wy) {
            raise = salary * 0.1;
        } else {
            raise = salary * 0.15;
        }

        return raise;
    }
}
