package com.mehmetemredogan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String name;
        double salary;
        int workHours;
        int hireYear;

        Scanner input = new Scanner(System.in);

        System.out.print("Çalışan Adı: ");
        name = input.nextLine();

        System.out.print("Maaş: ");
        salary = input.nextDouble();

        System.out.print("Haftalık Çalışma Saati: ");
        workHours = input.nextInt();

        System.out.print("İşe Giriş Yılı: ");
        hireYear = input.nextInt();


        Employee emp = new Employee(name, salary, workHours, hireYear);

        emp.toString();
    }
}
