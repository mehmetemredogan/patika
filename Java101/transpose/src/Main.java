import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        int row = matrix.length;
        int column = 0;

        for (int[] i : matrix) {
            for (int j : i) {
                column++;
            }
        }
        column = column / row;

        int[][] transpose = new int[column][row];
        for(int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                transpose[j][i] = matrix[i][j];
            }
        }

        System.out.println("Input");
        matrix(matrix);
        System.out.println("Output");
        matrix(transpose);
    }

    public static void matrix(int[][] data) {
        for (int[] i : data) {
            for (int j : i) {
                System.out.print(j + " ");
            }
            System.out.print("\n");
        }
    }
}