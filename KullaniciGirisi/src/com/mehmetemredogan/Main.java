package com.mehmetemredogan;

import java.util.Objects;
import java.util.Scanner;

public class Main {
    public static String RightPassword = "patika.dev";

    public static void main(String[] args) {
        login();
    }

    public static void login() {
        String username, password;

        Scanner input = new Scanner(System.in);

        System.out.print("Kullanıcı Adı: ");
        username = input.nextLine();

        System.out.print("Parola: ");
        password = input.nextLine();

        if (Objects.equals(username, "admin") && Objects.equals(password, RightPassword)) {
            System.out.print("Hoşgeldiniz!");
        } else {
            System.out.print("Kullanıcı Adınız ve/veya Parolanız Hatalı!");
            System.out.print("Şifrenizi unuttunuz mu? (evet/hayır)");

            String passwordResetQuestion = input.nextLine();

            if (Objects.equals(passwordResetQuestion, "hayır")) {
                System.out.print("Hoşçakal :)");
                System.exit(1);
            }

            passwordReset();
        }
    }

    public static void passwordReset() {
        Scanner input = new Scanner(System.in);

        System.out.print("Yeni parolanızı girin: ");
        String newPassword = input.nextLine();

        if (Objects.equals(newPassword, RightPassword)) {
            System.out.print("Yeni parolanız eskisiyle aynı olamaz!");
            System.exit(1);
        } else {
            RightPassword = newPassword;
            System.out.print("Parolanız Güncellendi!\n");
            login();
        }
    }
}
