package com.mehmetemredogan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int a, b, c, sayiA = 0, sayiB = 0, sayiC = 0;

        Scanner input = new Scanner(System.in);

        System.out.print("A sayısını girin: ");
        a = input.nextInt();

        System.out.print("B sayısını girin: ");
        b = input.nextInt();

        System.out.print("C sayısını girin: ");
        c = input.nextInt();

        if (a == b || a == c || b == c) {
            System.out.print("Sayılar eşit olamaz");
            System.exit(-1);
        }

        // En Küçük Sayı
        if ((a < b) && (a < c)) {
            sayiA = a;
        } else if ((b < a) && (b < c)) {
            sayiA = b;
        } else {
            sayiA = c;
        }

        // Ortanca Sayı
        if ((a > b) && (a < c)) {
            sayiB = a;
        } else if((a < b) && (a > c)) {
            sayiB = a;
        } else if ((b > a) && (b < c)) {
            sayiB = b;
        } else if((b < a) && (b > c)) {
            sayiB = b;
        } else if (c > a) {
            sayiB = c;
        } else {
            sayiB = c;
        }

        // En Büyük Sayı
        if ((a > b) && (a > c)) {
            sayiC = a;
        } else if ((b > a) && (b > c)) {
            sayiC = b;
        } else {
            sayiC = c;
        }

        System.out.printf("%d < %d < %d", sayiA, sayiB, sayiC);
    }
}
