package com.mehmetemredogan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int matematik, fizik, kimya, turkce, muzik;
        double toplam = 0, say = 0, ortalama;

        Scanner input = new Scanner(System.in);

        System.out.print("Matematik Notu: ");
        matematik = input.nextInt();

        System.out.print("Fizik Notu: ");
        fizik = input.nextInt();

        System.out.print("Kimya Notu: ");
        kimya = input.nextInt();

        System.out.print("Türkçe Notu: ");
        turkce = input.nextInt();

        System.out.print("Müzik Notu: ");
        muzik = input.nextInt();

        if (matematik > 0 && matematik < 100) {
            toplam += matematik;
            say++;
        }

        if (fizik > 0 && fizik < 100) {
            toplam += fizik;
            say++;
        }

        if (kimya > 0 && kimya < 100) {
            toplam += kimya;
            say++;
        }

        if (turkce > 0 && turkce < 100) {
            toplam += turkce;
            say++;
        }

        if (muzik > 0 && muzik < 100) {
            toplam += muzik;
            say++;
        }

        ortalama = (toplam / say);
        System.out.printf("%f ortalama ile ", ortalama);

        if (ortalama > 55) {
            System.out.print("geçti");
        } else  {
            System.out.print("kaldı");
        }
    }
}
