package com.mehmetemredogan;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        final double pi = 3.16;
        int r;

        Scanner input = new Scanner(System.in);
        System.out.print("Yarıçap girin: ");

        r = input.nextInt();

        double alan = pi * r * r;
        double cevre = 2 * pi * r;

        System.out.println("Alan: " + alan);
        System.out.println("Çevre: " + cevre);

        // Ödev (Yarıçapı r, merkez açısının ölçüsü 𝛼 olan daire diliminin alanı bulan program)
        System.out.print("Merkez açısını girin: ");
        int ma = input.nextInt();

        alan = ((pi * (r * r) * ma) / 360);

        System.out.printf("Yarıçapı %d, merkez açısı %d olan dairenin alanı: %f", r, ma, alan);

        input.close();
    }
}