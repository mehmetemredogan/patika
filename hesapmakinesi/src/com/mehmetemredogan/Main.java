package com.mehmetemredogan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        double numA, numB, out = 0;
        int operation;

        Scanner input = new Scanner(System.in);
        System.out.print("İlk Sayıyı Girin: ");
        numA = input.nextInt();

        System.out.print("İkinci Sayıyı Girin: ");
        numB = input.nextInt();

        System.out.print(
                """
                        Yapmak İstediğiniz İşlemi Seçin
                        1 - Toplama
                        2 - Çarpma
                        3 - Çıkarma
                        4 - Bölme
                        """
        );
        operation = input.nextInt();

        switch (operation) {
            case 1 -> out = (numA + numB);
            case 2 -> out = (numA * numB);
            case 3 -> out = (numA - numB);
            case 4 -> out = (numA / numB);
            default -> System.out.print("Bu sayıya karşılık gelen bir işlem yok :(");
        }

        System.out.printf("Sonuç: %f", out);
    }
}
