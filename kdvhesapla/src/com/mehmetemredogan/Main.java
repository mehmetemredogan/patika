package com.mehmetemredogan;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        double kdv = 0.18, fiyat;

        Scanner fiyatInput = new Scanner(System.in);

        System.out.print("Ürün fiyatını girin: ");
        fiyat = fiyatInput.nextDouble();

        if (fiyat >= 1000.0) {
            kdv = 0.08;
        }

        System.out.println("KDV hariç fiyat: " + fiyat);
        System.out.println("KDV dahil fiyat: " + (fiyat + (fiyat * kdv)));
        System.out.println("KDV oranı: " + (kdv * 100));
    }
}
