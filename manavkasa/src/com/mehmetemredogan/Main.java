package com.mehmetemredogan;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        double  armut       = 2.14,
                elma        = 3.67,
                domates     = 1.11,
                muz         = 0.95,
                patlican    = 5.00,
                armutToplam, elmaToplam, domatesToplam, muzToplam, patlicanToplam;

        Scanner input = new Scanner(System.in);

        System.out.print("Armut Kaç Kilo? :");
        armutToplam = input.nextInt() * armut;

        System.out.print("Elma Kaç Kilo? :");
        elmaToplam = input.nextInt() * elma;

        System.out.print("Domates Kaç Kilo? :");
        domatesToplam = input.nextInt() * domates;

        System.out.print("Muz Kaç Kilo? :");
        muzToplam = input.nextInt() * muz;

        System.out.print("Patlıcan Kaç Kilo? :");
        patlicanToplam = input.nextInt() * patlican;

        System.out.println("Toplam Tutar: " + (armutToplam + elmaToplam + domatesToplam + muzToplam + patlicanToplam) + " TL");
    }
}