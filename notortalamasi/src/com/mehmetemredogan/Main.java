package com.mehmetemredogan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int matematik, fizik, kimya, biyoloji, edebiyat, cografya, felsefe, din, tarih, turkce;

        Scanner input = new Scanner(System.in);

        System.out.print("Matematik Notu: ");
        matematik = input.nextInt();

        System.out.print("Fizik Notu: ");
        fizik = input.nextInt();

        System.out.print("Kimya Notu: ");
        kimya = input.nextInt();

        System.out.print("Biyoloji Notu: ");
        biyoloji = input.nextInt();

        System.out.print("Türk Edebiyatı Notu: ");
        edebiyat = input.nextInt();

        System.out.print("Coğrafya Notu: ");
        cografya = input.nextInt();

        System.out.print("Felsefe Notu: ");
        felsefe = input.nextInt();

        System.out.print("Din Kültürü ve Ahlak Bilgisi Notu: ");
        din = input.nextInt();

        System.out.print("Tarih Notu: ");
        tarih = input.nextInt();

        System.out.print("Türkçe Notu: ");
        turkce = input.nextInt();

        int toplam = (matematik + fizik + kimya + biyoloji + edebiyat + cografya + felsefe + din + tarih + turkce);

        System.out.println("Ortalamanız: " + (toplam / 10));

    }
}
