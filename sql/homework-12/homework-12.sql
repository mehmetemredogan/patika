-- 1 (489)
SELECT
	count(*)
FROM
	film
WHERE
	length >
(
	SELECT
		avg(length)
	FROM
		film
);

-- 2 (336)
SELECT
	count(rental_rate)
FROM
	film
WHERE
	rental_rate =
(
	SELECT
		max(rental_rate)
	FROM
		film
);

-- 3
SELECT
	*
FROM
	film
WHERE
	rental_rate =
(
	SELECT
		min(rental_rate)
	FROM
		film
)
	AND replacement_cost =
(
	SELECT
		min(replacement_cost)
	FROM
		film
);

-- 4
SELECT
	DISTINCT
	payment.customer_id,
	customer.first_name,
	customer.last_name,
	(
		SELECT
			count(*)
		FROM
			payment
		WHERE
			payment.customer_id = customer.customer_id
	) AS payment_count
FROM
	payment
JOIN customer ON
	customer.customer_id = payment.customer_id
ORDER BY
	payment_count
DESC;