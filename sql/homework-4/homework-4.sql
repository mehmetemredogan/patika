-- 1
SELECT
	DISTINCT replacement_cost 
FROM
	film;

-- 2
SELECT
	count(DISTINCT replacement_cost)
FROM
	film;

-- 3 (9)
SELECT
	count(title)
FROM
	film
WHERE title LIKE 'T%' AND rating = 'G';

-- 4 (13)
SELECT
	count(country)
FROM
	country
WHERE country LIKE '_____';

-- 5 (33)
SELECT
	count(city)
FROM
	city
WHERE city LIKE '%r';