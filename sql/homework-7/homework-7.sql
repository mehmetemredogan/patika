-- 1
SELECT
	rating
FROM
	film
GROUP BY
	rating;

-- 2
SELECT
	replacement_cost,
	COUNT(replacement_cost)
FROM
	film
GROUP BY
	replacement_cost
HAVING
	COUNT(*) > 50;

-- 3
SELECT
	store_id,
	count(store_id) 
FROM
	customer
GROUP BY
	store_id;

-- 4 (country_id: 44 (India), city count: 60)
SELECT
	country_id,
	COUNT(city_id)
FROM
	city
GROUP BY
	country_id
ORDER BY
	COUNT(city_id) DESC;