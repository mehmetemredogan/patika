-- 1
CREATE SEQUENCE employee_id_seq;

CREATE TABLE employee(
	id INTEGER PRIMARY KEY DEFAULT nextval('employee_id_seq'),
	name VARCHAR(50),
	birthday DATE,
	email VARCHAR(100)
);

ALTER SEQUENCE employee_id_seq OWNED BY employee.id;

-- 2
insert into employee (id, name, birthday, email) values (1, 'Verla Jewitt', '1999-06-16', 'vjewitt0@google.de');
insert into employee (id, name, birthday, email) values (2, 'Hollis Creasy', '1994-10-19', 'hcreasy1@ft.com');
insert into employee (id, name, birthday, email) values (3, 'Perl Eyers', '1992-10-24', 'peyers2@youku.com');
insert into employee (id, name, birthday, email) values (4, 'Luz Sothern', '1998-07-05', 'lsothern3@delicious.com');
insert into employee (id, name, birthday, email) values (5, 'Olivette Girardoni', '1997-05-20', 'ogirardoni4@ox.ac.uk');
insert into employee (id, name, birthday, email) values (6, 'Persis Bleasdille', '1999-04-24', 'pbleasdille5@dailymail.co.uk');
insert into employee (id, name, birthday, email) values (7, 'Nathanil Benion', '1996-04-03', 'nbenion6@redcross.org');
insert into employee (id, name, birthday, email) values (8, 'Romain Milkin', '1994-03-01', 'rmilkin7@dailymotion.com');
insert into employee (id, name, birthday, email) values (9, 'Lorry Knotte', '1999-12-15', 'lknotte8@jugem.jp');
insert into employee (id, name, birthday, email) values (10, 'Hasty McKimm', '1993-04-19', 'hmckimm9@unblog.fr');
insert into employee (id, name, birthday, email) values (11, 'Gardener Roxbee', '1991-03-25', 'groxbeea@istockphoto.com');
insert into employee (id, name, birthday, email) values (12, 'Fielding Lark', '1992-03-30', 'flarkb@tuttocitta.it');
insert into employee (id, name, birthday, email) values (13, 'Wright Gabbat', '1992-07-05', 'wgabbatc@sakura.ne.jp');
insert into employee (id, name, birthday, email) values (14, 'Derk Paddell', '1992-12-31', 'dpaddelld@sourceforge.net');
insert into employee (id, name, birthday, email) values (15, 'Lamont Mealand', '1992-03-06', 'lmealande@ebay.com');
insert into employee (id, name, birthday, email) values (16, 'Constance Duell', '1994-08-16', 'cduellf@pinterest.com');
insert into employee (id, name, birthday, email) values (17, 'Cloris Harkness', '1997-07-29', 'charknessg@naver.com');
insert into employee (id, name, birthday, email) values (18, 'Cesya Brunel', '1999-06-12', 'cbrunelh@dailymail.co.uk');
insert into employee (id, name, birthday, email) values (19, 'Kelcey Dyton', '1998-04-30', 'kdytoni@youku.com');
insert into employee (id, name, birthday, email) values (20, 'Denny Bucklee', '1993-12-23', 'dbuckleej@bravesites.com');
insert into employee (id, name, birthday, email) values (21, 'Rosana Burde', '1999-01-24', 'rburdek@shutterfly.com');
insert into employee (id, name, birthday, email) values (22, 'Dame Houlston', '2000-07-15', 'dhoulstonl@boston.com');
insert into employee (id, name, birthday, email) values (23, 'Clemens O''Henery', '1998-08-01', 'cohenerym@360.cn');
insert into employee (id, name, birthday, email) values (24, 'Wendell Lougheed', '1996-09-07', 'wlougheedn@mediafire.com');
insert into employee (id, name, birthday, email) values (25, 'Dorri Crolla', '1991-02-06', 'dcrollao@free.fr');
insert into employee (id, name, birthday, email) values (26, 'Georgianne Smitton', '1997-04-27', 'gsmittonp@usa.gov');
insert into employee (id, name, birthday, email) values (27, 'Vikki Littlefair', '1997-03-09', 'vlittlefairq@ameblo.jp');
insert into employee (id, name, birthday, email) values (28, 'Mariam Schrieves', '1997-08-29', 'mschrievesr@godaddy.com');
insert into employee (id, name, birthday, email) values (29, 'Emmott Pardue', '1997-05-11', 'epardues@mapquest.com');
insert into employee (id, name, birthday, email) values (30, 'Alaric Newgrosh', '1994-12-08', 'anewgrosht@yahoo.co.jp');
insert into employee (id, name, birthday, email) values (31, 'Claudianus Kerley', '1995-11-23', 'ckerleyu@msu.edu');
insert into employee (id, name, birthday, email) values (32, 'Ericha Botley', '1994-09-06', 'ebotleyv@mail.ru');
insert into employee (id, name, birthday, email) values (33, 'Sydelle Gjerde', '1995-01-22', 'sgjerdew@independent.co.uk');
insert into employee (id, name, birthday, email) values (34, 'Olivie Piris', '1995-01-26', 'opirisx@accuweather.com');
insert into employee (id, name, birthday, email) values (35, 'Leigha Curnick', '1992-06-10', 'lcurnicky@apache.org');
insert into employee (id, name, birthday, email) values (36, 'Charlene Feldberg', '1991-08-01', 'cfeldbergz@ed.gov');
insert into employee (id, name, birthday, email) values (37, 'Leodora McCarlie', '1992-05-25', 'lmccarlie10@merriam-webster.com');
insert into employee (id, name, birthday, email) values (38, 'Kippie Mattiazzo', '1999-09-07', 'kmattiazzo11@1und1.de');
insert into employee (id, name, birthday, email) values (39, 'Angie Pandey', '2000-06-03', 'apandey12@domainmarket.com');
insert into employee (id, name, birthday, email) values (40, 'Bord Ianizzi', '1994-08-14', 'bianizzi13@yellowpages.com');
insert into employee (id, name, birthday, email) values (41, 'Loydie Vinker', '1993-12-26', 'lvinker14@google.ru');
insert into employee (id, name, birthday, email) values (42, 'Miquela Fudge', '1993-07-21', 'mfudge15@nature.com');
insert into employee (id, name, birthday, email) values (43, 'Felic Elby', '1996-10-05', 'felby16@toplist.cz');
insert into employee (id, name, birthday, email) values (44, 'Edvard Falconar', '1991-12-10', 'efalconar17@smh.com.au');
insert into employee (id, name, birthday, email) values (45, 'Mindy Sinden', '1996-01-07', 'msinden18@dagondesign.com');
insert into employee (id, name, birthday, email) values (46, 'Ruddy Flamank', '2000-05-29', 'rflamank19@guardian.co.uk');
insert into employee (id, name, birthday, email) values (47, 'Jolene Seaward', '1998-12-30', 'jseaward1a@foxnews.com');
insert into employee (id, name, birthday, email) values (48, 'Charmion Galletley', '1995-08-08', 'cgalletley1b@xrea.com');
insert into employee (id, name, birthday, email) values (49, 'Janetta Penswick', '1995-11-23', 'jpenswick1c@nyu.edu');
insert into employee (id, name, birthday, email) values (50, 'Duffy Lordon', '1997-11-16', 'dlordon1d@senate.gov');

-- 3
update employee set email = 'verlajewitt@vjjjj.com' where id = 1;
update employee set email = 'mehmetemredogan@protonmail.com' where id = 50;
update employee set name = 'Mehmet Emre Doğan' where email = 'mehmetemredogan@protonmail.com';
update employee set birthday = '1998-06-27' where name = 'Mehmet Emre Doğan';
update employee set email = 'felby161@toplist.cz' where email = 'felby16@toplist.cz';

-- 4
delete from employee where id = 2;
delete from employee where name = 'Perl Eyers';
delete from employee where birthday = '1998-07-05';
delete from employee where email = 'lknotte8@jugem.jp';
delete from employee where email like '%.gov';