-- 1
SELECT
	city.city,
	country.country
FROM
	city
INNER JOIN country ON
	country.country_id = city.country_id;

-- 2
SELECT
	payment.payment_id,
	customer.first_name,
	customer.last_name
FROM
	customer
JOIN payment ON
	payment.customer_id = customer.customer_id;

-- 3
SELECT
	rental.rental_id,
	customer.first_name,
	customer.last_name
FROM
	customer
JOIN rental ON
	rental.customer_id = customer.customer_id;