package com.mehmetemredogan;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        double boy, kilo;

        Scanner input = new Scanner(System.in);
        System.out.print("Boyunuzu girin (metre): ");
        boy = input.nextDouble();
        System.out.print("Kilonuzu girin (kg): ");
        kilo = input.nextDouble();

        double vki = kilo / (boy * boy);

        System.out.println("Vücut kitle indeksiniz: " + vki);
        input.close();
    }
}